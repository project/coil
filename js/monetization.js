(function ($, Drupal, drupalSettings) {

    if (document.monetization) {

        document.monetization.addEventListener('monetizationstart', function (event) {
            $.ajax({
                type: 'POST',
                url: drupalSettings.path.baseUrl + "monetized-content",
                data: {path: drupalSettings.path.currentPath, element: drupalSettings.monetized_element},
                dataType: 'json',
                success: function success(data) {
                    $(".field--name-field-" + drupalSettings.monetized_element).html(data.markup);
                },
                error: function error(xmlhttp) {
                    var e = new Drupal.AjaxError(xmlhttp, drupalSettings.path.baseUrl + "monetized-content");
                    var error = $('<div class="messages messages--error"></div>').html('<pre>' + e.message + '</pre>');
                    $(".field--name-field-" + drupalSettings.monetized_element).before(error).hide();
                }
            });
        })
    }


})(jQuery, Drupal, drupalSettings);

