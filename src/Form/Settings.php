<?php

/**
 * @file
 * Contains \Drupal\coil\Form\Settings.
 */

namespace Drupal\coil\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure coil settings for this site.
 */
class Settings extends ConfigFormBase {

    /** @var string Config settings */
    const SETTINGS = 'coil.settings';

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'coil_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            static::SETTINGS,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config(static::SETTINGS);

        $form['deposit'] = [
            '#type' => 'radios',
            '#options' => ['twitter' => '$twitter', 'coil' => '$coil'],
            '#default_value' => null !== \Drupal::config('coil.settings')->get('coil_deposit') ? \Drupal::config('coil.settings')->get('coil_deposit') : 'twitter',
        ];

        $form['pointer'] = [
            '#type' => 'textfield',
            '#required' => true,
            '#title' => $this->t('Coil pointer'),
            '#attributes' => ['placeholder' => $this->t('my_pointer')],
            '#default_value' => \Drupal::config('coil.settings')->get('coil_pointer'),
            '#description' => $this->t('Do not include "$twitter.xrptipbot.com/" or "$coil.xrptipbot.com/" in your address. See <a href="@c">coil</a>', ['@c' => 'https://coil.com/settings/creator'])
        ];

        $node_types = \Drupal\node\Entity\NodeType::loadMultiple();
        $options = [];
        foreach ($node_types as $node_type) {
            $options[$node_type->id()] = $node_type->label();
        }

        $form['content'] = [
            '#type' => 'checkboxes',
            '#options' => $options,
            '#default_value' => \Drupal::config('coil.settings')->get('content'),
            '#title' => $this->t('Monetize restricted content in') . ':'
        ];

        $form['custom_alert'] = [
            '#type' => 'textarea',
            '#default_value' => \Drupal::config('coil.settings')->get('custom_alert'),
            '#attributes' => array('placeholder' => $this->t('The following content is restricted for web monetization suscribers.')),
            '#title' => $this->t('Custom alert for restricted content')
        ];

        $form['custom_link'] = [
            '#type' => 'textfield',
            '#maxlength' => 50,
            '#default_value' => \Drupal::config('coil.settings')->get('custom_link'),
            '#attributes' => array('placeholder' => $this->t('Get started with Coil')),
            '#title' => $this->t('Custom text for subscription link')
        ];

        $form['alert_color'] = [
            '#type' => 'color',
            '#title' => $this->t('Color for subscription alert box'),
            '#default_value' => \Drupal::config('coil.settings')->get('alert_color_hex'),
        ];


        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        preg_match('/\$twitter.xrptipbot.com\//', $form_state->getValue('pointer'), $matches);

        if (!empty($matches)) {
            $form_state->setErrorByName('pointer', $this->t('Do not include "$twitter.xrptipbot.com/" in your address. See <a href="@c">coil</a>', ['@c' => 'https://coil.com/creator-setup']));
        }
        preg_match('/\$coil.xrptipbot.com\//', $form_state->getValue('pointer'), $matches);

        if (!empty($matches)) {
            $form_state->setErrorByName('pointer', $this->t('Do not include "$coil.xrptipbot.com/" in your address. See <a href="@c">coil</a>', ['@c' => 'https://coil.com/creator-setup']));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $c = \Drupal\Component\Utility\Color::hexToRgb($form_state->getValue('alert_color'));
        // Retrieve the configuration
        $this->configFactory->getEditable(static::SETTINGS)
                // Set the submitted configuration setting
                ->set('coil_pointer', $form_state->getValue('pointer'))
                ->set('coil_deposit', $form_state->getValue('deposit'))
                ->set('content', $form_state->getValue('content'))
                ->set('custom_alert', $form_state->getValue('custom_alert'))
                ->set('custom_link', $form_state->getValue('custom_link'))
                ->set('alert_color_hex', $form_state->getValue('alert_color'))
                ->set('alert_color_rgb', $c['red'] . ',' . $c['green'] . ',' . $c['blue'])
                ->save();

        parent::submitForm($form, $form_state);
    }

}
