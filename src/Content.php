<?php

/**
 * @file
 * Contains \Drupal\coil\Content.
 *
 */

namespace Drupal\coil;

use Symfony\Component\HttpFoundation\Request;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\JsonResponse;

class Content {

    public function monetized(Request $request) {
        $path = $request->get('path');
        $element = str_replace('-', '_', $request->get('element'));
        $parts = explode('/', $path);
        $nid = $parts[1];
        $node = Node::load($nid);
        $value = [
            '#markup' => $node->get('field_' . $element)->value,
        ];
        
        $markup = \Drupal::service('renderer')->render($value);
        return new JsonResponse(['markup' => $markup]);
        
    }

}
