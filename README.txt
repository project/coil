Coil implementation for Drupal

## usage

After module is installed, to activate your coil account, go to [configuration] (/admin/coil).
Register your coil pointer in the site configuration.

For further information go to [coil website] (https://coil.com)

## tip

If this module is useful, please consider small tip at $twitter.xrptipbot.com/arrea_systems
